<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=450, initial-scale=.7">
    <link rel="icon" href="img/bd_brasil.png" />
    <title>Bolão 2018 - Galera</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/sweetalert2.min.js"></script>
  </head>
  <body>
    <style>
      body {
        background-image: url("./img/bg.png");
        background-repeat: repeat;
      }
      @media screen and (max-width: 650px) {
        .e1inp, .e2inp{
          width: 50px;
        }
      }
      .bd{
        background-color: white;
      }
      .bdf {
        padding-left: 10px;
        padding-right: 10px;
        background-color: white;
        border: 1px solid;
        border-radius: 10px;
        margin-top: 5px;
        margin-bottom: 5px;
      }
      /*th{
      	border: 1px dashed #c3f;
	      font-size: 10px;
	      text-align: center;
      }*/
      td{
        padding-top: 13px;
        border: 1px dashed #c3f;
      }
      div .bd{
        border: 1px solid;
        border-radius: 10px;
        margin-top: 5px;
        margin-bottom: 5px;
      }
      .e1, .e2, .versus{
        font-size: 20px;
        text-align: center;
      }
      .lineAp{
      }
      .palpiteiro{
        margin-top: -5px;
        font-size: 15px;
      }
      .clf tr{
        padding: 10px;
      }
      .clf td{
        padding: 10px;
        text-align: center;
      }
      .green {
        color: green;
      }
      .orange {
        color: orange;
      }
      .grey {
        color: grey;
      }
      .blue{
        color: blue;
      }
      .red{
        color: red;
      }
      .tbestat td{
        padding: 8px;
      }
    </style>
    <script>
    function palpitar(id){
      let e1 = $("#e1_"+id).val();
      let e2 = $("#e2_"+id).val();
      let aposta = $("#aposta_"+id).val();
      //console.log(e1, e2);

      let id2 = new String(id);
      let str = id2.split('_');
      let jogador = str[1];
      let isPalpite = true;

      if(aposta != ""){
        let pontos = parseInt($("#pontos_"+jogador).text());
        valor_aposta = aposta;
        if(parseInt(valor_aposta) > pontos){
          isPalpite = false;
          swal({
            title: 'Espertinho hein!!',
            text: 'Tentando apostar mais pontos do que possui!',
            type: 'error',
            confirmButtonText: 'Ok'
          });
        }
      }

      if(isPalpite){
        $.ajax(
          {
            type: "GET",
            url: "./writeResult.php?id="+id+"&e1="+e1+"&e2="+e2+"&aposta="+aposta,
            success: function(result){
              let obj = JSON.parse(result);
              if(obj.rst === true){
                swal({
                  title: 'Hmmm... que delícia!',
                  text: 'Palpite anotado para o jogador: ' + obj.jogador,
                  type: 'success',
                  confirmButtonText: 'Ok'
                });
              }else if(obj.rst === false){
                swal({
                  title: 'Informe os valores do placar!',
                  text: 'Tentando anotar o palpite do jogador: ' + obj.jogador,
                  type: 'info',
                  confirmButtonText: 'Ok'
                });
              }else if(obj.rst === -1){
                swal({
                  title: 'Problema ao anotar o resultado!',
                  text: 'Não foi possível salvar seu placar. Entre em contato com o programador que fez isso. ',
                  type: 'info',
                  confirmButtonText: 'Ok'
                });
              }
            }
          }
        );
      }
    }
    $( document ).ready(function() {
      console.log( "Pronto!" );
    });
    </script>

    <div class="container">
      <div class="bd col-offset-6 centered" style="text-align:center; padding: 10px;">
        <h2>Bolão da Galera - 2018</h2>

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="btCalculaResultado">
          Classificação
        </button>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal3" id="btStat">
          Estatísticas
        </button>
        <a href="https://pt.wikipedia.org/wiki/Estat%C3%ADsticas_da_Copa_do_Mundo_FIFA_de_2018" target="_blank">
          <button type="button" class="btn btn-success">
            Estatísticas copa
          </button>
        </a>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2" id="btPremios">
          Prêmio
        </button>
      </div>
      <div class="row">&nbsp;</div>
      <?php
      /*require "vendor/autoload.php";
      use PHPHtmlParser\Dom;

      $dom = new Dom;
      $dom->loadFromUrl("https://pt.wikipedia.org/wiki/Estat%C3%ADsticas_da_Copa_do_Mundo_FIFA_de_2018");
      $tabela_dados = $dom->find('.wikitable')[1];
      $legenda = $dom->find('small')[0];
      $html_estat_copa = str_replace('/wiki/', 'https://pt.wikipedia.org/wiki/',$tabela_dados->outerHtml);
      $html_estat_copa .= $legenda;
      */
        $hand = fopen('./log.txt', 'a+');
        $log = date("d/m/Y H:i:s")."#".$_SERVER['REMOTE_ADDR']."#".$_SERVER['HTTP_USER_AGENT']."\r\n";
        fwrite($hand, $log);
        fclose($hand);

        require_once('./class/Template.class.php');
        $tpl = new Template("./tabela.html");
        $strfases = file_get_contents('./js/fases.json');
        $strjogos = file_get_contents('./js/jogos.json');
        $strapost = file_get_contents('./js/apostadores.json');
        $tabelaFases = json_decode($strfases, true);
        $tabelaJogos = json_decode($strjogos, true);
        $apostadores = json_decode($strapost, true);
      //  print_r($tabelaJogos);
      //  print_r($apostadores);
       $ESTAT_NUM_JOGOS = 0;
       $ESTAT_NUM_JOGOS_ENCERRADOS = 0;

       for($k = 1; $k <= count($apostadores); $k++){
         $parp = "A".$k;
         $ESTAT_PP_JOGOS_0PONTO[$parp] = 0;
         $ESTAT_PP_JOGOS_1PONTO[$parp] = 0;
         $ESTAT_PP_JOGOS_3PONTO[$parp] = 0;
         $array_jogador_pontos[$parp] = 0;
         $array_jogador_pontos_totais[$parp] = 0;
         $array_jogador_pontos_ganhos_aposta[$parp] = 0;
       }

       //pega o numero de fases
       for($f = 1; $f <= count($tabelaFases); $f++){
         $faseid = "F".$f;

         //pega o numero de dias daquela fase
         for($i = 1; $i <= count($tabelaJogos[$faseid]); $i++){

           $diaid = "d".$i;

           $tpl->DATA = $tabelaJogos[$faseid][$diaid][1];

           //pega o numero de jogos da fase naquele dia que está no próprio arquivo json
           for($j = 1; $j <= intval($tabelaJogos[$faseid][$diaid][0]); $j++){
             $ESTAT_NUM_JOGOS += 1;

             //pega o numero de apostadores (vem do arquivo json)
             for($k = 1; $k <= count($apostadores); $k++){

               $parpiteiro = "A".$k;

               $rotulo_jogo = $tabelaJogos[$faseid][$diaid][$j+1]["rot"];
               $tpl->PALPITEIRO = "[".$parpiteiro."] - ".$apostadores[$parpiteiro]["nome"];
               //$tpl->DIA = $i;
               $tpl->JOGOROT = $rotulo_jogo;
               $tpl->PALP = $parpiteiro;

               $file = './dados/palpiteiro_'.$parpiteiro."_".$rotulo_jogo.".txt";
               if(file_exists($file)){
                 $placar_palpiteiro = @file_get_contents($file);
                 $palp_placar = explode('x', preg_replace('/[^0-9x\-]/', '', $placar_palpiteiro));
                 $pe1 = $palp_placar[0];
                 $pe2 = $palp_placar[1];

                 $tpl->VALUE_E1 = $pe1;
                 $tpl->VALUE_E2 = $pe2;
               }else{
                 $pe1 = "";
                 $pe2 = "";
                 $tpl->VALUE_E1 = "";
                 $tpl->VALUE_E2 = "";
               }
               //fase maior = 2 liberar apostas
               if($f >= 2){
                 $pontos_apostados = "";
                 $file2 = './dados/aposta_'.$parpiteiro."_".$rotulo_jogo.".txt";
                 if(file_exists($file2)){
                   $pontos_apostados = intval(@file_get_contents($file2));
                   $tpl->VALUE_APOSTA = $pontos_apostados;
                 }else{
                    $tpl->VALUE_APOSTA = "";
                 }
                 $tpl->block("CAMPO_APOSTAS");
               }

               if($tabelaJogos[$faseid][$diaid][$j+1]["placar"][0] != -1){
                 $tpl->ATTRIB_PALP_E1 = 'data-palpiteiro="'.$parpiteiro."_".$rotulo_jogo.'_E1"';
                 //$tpl->ATTRIB_PALP_E2 = 'data-palpiteiro="'.$parpiteiro."_".$rotulo_jogo.'_E2"';
                 $tpl->BT_DISABLED = "disabled=disabled";

                 $ple1 = $tabelaJogos[$faseid][$diaid][$j+1]["placar"][0];
                 $ple2 = $tabelaJogos[$faseid][$diaid][$j+1]["placar"][1];
                 $pontos = -1;
                 if($pe1 == $ple1 && $pe2 == $ple2){
                   $pontos = 3;
                 }
                 else if(($pe1 == $pe2) && (($ple1 > $ple2) || ($ple1 < $ple2))){
                   $pontos = 0;
                 }
                 else if(($pe1 > $pe2) && ($ple1 < $ple2)){
                   $pontos = 0;
                 }
                 else if(($pe1 < $pe2) && ($ple1 > $ple2)){
                   $pontos = 0;
                 }
                 else if($ple1 == $ple2 && ($pe1 > $pe2 || $pe2 > $pe1)){
                   $pontos = 0;
                 }else if($pe1 == "" || $pe2 == ""){
                   $pontos = 0;
                 }
                 else{
                   $pontos = 1;
                 }

                 $array_jogador_pontos[$parpiteiro] += $pontos;

                 $tpl->PONTO_MARCADO_NO_JOGO = $pontos .' pts';

                 //fase maior = 2 liberar apostas
                 if($f >= 2){

                   $pontos_ganhos_aposta = 0;

                   //regra de pontuacao para as fases acima de 2
                   if($pontos === 0){
                     $pontos_ganhos_aposta = -$pontos_apostados;
                   }else if($pontos === 3){
                     $fator = 2;
                     $pontos_ganhos_aposta = $fator * $pontos_apostados;
                   }

                   if($pontos_ganhos_aposta > 0){
                     $tpl->CSS_COLOR_APOSTA = "blue";
                     $tpl->PONTO_MARCADO_NA_APOSTA = ' (+'.$pontos_ganhos_aposta.' pts)';
                   }else if($pontos_ganhos_aposta < 0){
                     $tpl->CSS_COLOR_APOSTA = "red";
                     $tpl->PONTO_MARCADO_NA_APOSTA = ' ('.$pontos_ganhos_aposta.' pts)';
                   }else{
                     $tpl->CSS_COLOR_APOSTA = "grey";
                     $tpl->PONTO_MARCADO_NA_APOSTA = ' (+'.$pontos_ganhos_aposta.' pts)';
                   }

                   $array_jogador_pontos_ganhos_aposta[$parpiteiro] += $pontos_ganhos_aposta;

                 }else{
                   $tpl->PONTO_MARCADO_NA_APOSTA = "";
                   $tpl->CSS_COLOR_APOSTA = "";
                 }


                 if($pontos === 0){
                   $ESTAT_PP_JOGOS_0PONTO[$parpiteiro] += 1;
                   $css = 'grey';
                 }else if($pontos === 1){
                   $ESTAT_PP_JOGOS_1PONTO[$parpiteiro] += 1;
                   $css = 'green';
                 }else if ($pontos === 3){
                   $ESTAT_PP_JOGOS_3PONTO[$parpiteiro] += 1;
                   $css = 'orange';
                 }

                 $tpl->CSS_COLOR = $css;

               } else{
                 $tpl->ATTRIB_PALP_E1 = "";
                 $tpl->BT_DISABLED = "";
                 //$tpl->ATTRIB_PALP_E2 = "";
                 $tpl->PONTO_MARCADO_NO_JOGO = "";
                 $tpl->CSS_COLOR = "";

                 $tpl->PONTO_MARCADO_NA_APOSTA = "";
                 $tpl->CSS_COLOR_APOSTA = "";
               }

               $tpl->block("ROW_PALPITEIROS");

            }

            $tpl->HORA =  $tabelaJogos[$faseid][$diaid][$j+1]["hora"];
            $tpl->EQUIPE1 = $tabelaJogos[$faseid][$diaid][$j+1]["e1"];
            $tpl->EQUIPE2 = $tabelaJogos[$faseid][$diaid][$j+1]["e2"];

            //echo $diaid." [".($j+1)."] = ".$tabelaJogos[$diaid][$j+1]["placar"][0];
            if($tabelaJogos[$faseid][$diaid][$j+1]["placar"][0] != -1){
              $ESTAT_NUM_JOGOS_ENCERRADOS += 1;
              $tpl->PLACAR_E1 = $tabelaJogos[$faseid][$diaid][$j+1]["placar"][0];
              $tpl->PLACAR_E2 = $tabelaJogos[$faseid][$diaid][$j+1]["placar"][1];
            } else{
              $tpl->PLACAR_E1 = '__';
              $tpl->PLACAR_E2 = '__';
            }

            $tpl->block("TABLE_GAME");
          }
          $tpl->ID = $faseid."_".$diaid;
          $tpl->block('BLK_PRINCIPAL');
        }


        $tpl->ID_FASE = $tabelaFases[$faseid]["id"];
        $tpl->FASE = $tabelaFases[$faseid]["label"];
        $tpl->block('BLK_FASE');
      }

  /* MONTAS AS ESTATÍSTICAS */

      //<td colspan="2">(JL) Jogos Listados: '.$ESTAT_NUM_JOGOS.'
      $estatisticas_html = '
        <table class="tbestat">
        <tbody>
            <tr>
              <td></td>
              <td colspan="4">Jogos encerrados: '.$ESTAT_NUM_JOGOS_ENCERRADOS.'
            <tr>
            <tr>
              <td>Palpiteiro</td>
              <td>acertos (%)</td>
              <td>Jogos fez 3 pts</td>
              <td>Jogos fez 1 pts</td>
              <td>Jogos fez 0 pts</td>
            </tr>
      ';

      for($k = 1; $k <= count($apostadores); $k++){
        $parpiteiro = "A".$k;
        $j0pt = $ESTAT_PP_JOGOS_0PONTO[$parpiteiro];
        $j1pt = $ESTAT_PP_JOGOS_1PONTO[$parpiteiro];
        $j3pt = $ESTAT_PP_JOGOS_3PONTO[$parpiteiro];
        $acertos = ($j1pt + $j3pt);

          $estatisticas_html .= '
            <tr>
              <td>'.'['.$parpiteiro.'] - '.$apostadores[$parpiteiro]["nome"].'</td>
              <td>'.$acertos.' ('.round(($acertos*100)/$ESTAT_NUM_JOGOS_ENCERRADOS, 2).'%)</td>
              <td>'.$j3pt.' ('.round(($j3pt*100)/$ESTAT_NUM_JOGOS_ENCERRADOS, 2).'%)</td>
              <td>'.$j1pt.' ('.round(($j1pt*100)/$ESTAT_NUM_JOGOS_ENCERRADOS, 2).'%)</td>
              <td>'.$j0pt.' ('.round(($j0pt*100)/$ESTAT_NUM_JOGOS_ENCERRADOS, 2).'%)</td>
            </tr>
            ';
      }

      $estatisticas_html .= '
        </tbody>
        </table>
      ';

      $html_classificacao = '
        <table class="clf">
          <tr>
            <td>Palpiteiro</td>
            <td>pN + pA</td>
            <td>pts Normal (pN)</td>
            <td>pts Aposta (pA)</td>
          </tr>
      ';

      arsort($array_jogador_pontos);

      foreach($array_jogador_pontos as $k => $v){
        $array_jogador_pontos_totais[$k] += $v + $array_jogador_pontos_ganhos_aposta[$k];
      }

      arsort($array_jogador_pontos_totais);

      foreach($array_jogador_pontos_totais as $k => $v){

        $html_classificacao .= '
          <tr>
            <td>'.'['.$k.'] - '.$apostadores[$k]["nome"].'</td>
            <td id="pontos_'.$k.'">'.$v.'</td>
            <td>'.$array_jogador_pontos[$k].'</td>
            <td>'.$array_jogador_pontos_ganhos_aposta[$k].'</td>
          </tr>
        ';
      }

      $html_classificacao .= '</table>';

      $tpl->show();
    ?>
    </div>

    <!-- Modal Classificacao -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Classificação delícia</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="classificacao">
              <?=$html_classificacao;?>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Estatísticas -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Desempenho dos deliciosos competidores!</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="estatisticas-da-galera">
              <?=$estatisticas_html;?>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Estatísticas Copa -->
    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Estatísticas dos jogos...</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="estatisticas-da-copa">
              <?=$html_estat_copa;?>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Premios -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Prêmios para os vencedores...</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <h2><u>1° Lugar:</u>	Noite de delícias com o Maiser</h2>
              <h2><u>2° Lugar:</u>	Serviço de mecânico do Maiser</h2>
              <h2><u>3° Lugar:</u>	Suco de laranja feito pelo Maiser</h2>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
